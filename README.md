# TP Snakemake et cluster

Ce TP a pour but de vous faire découvrir l'utilisation d'un cluster et du logiciel snakemake.

## Cluster de calcul

Imaginez que vous ayez une équipe de 20 personnes qui développe ou utilise des applications scientifiques parallèles et qui utilisent beaucoup de mémoire vive. Plusieurs contraintes apparaissent :
- le besoin de RAM et de processeurs est trop grand pour un pc de bureau (même bien équipé)
- chaque personne n'utilise pas son pc tout le temps (il y a du temps perdu)
- le coût de maintenance des pc pourrait être mutualisé

Le cluster de calcul répond à ces contraintes en mutualisant des pc stockés dans des armoires et gérés par un administrateur. Ainsi, chaque utilisateur peut, en fonction de ses besoins, accéder à des ressources de calcul conséquentes. Un gestionnaire de tâches vient répartir les calculs en fonction des demandes.

## Configurer sa clé publique sur Genocluster

Nous allons travailler sur genocluster, le cluster de calcul de la platefrome bioinformatique régionale Genouest. Pour se connecter, chacun d'entre-vous a reçu un couple login/password. Je vous invite donc à :
1. Vous connecter sur `genouest.org`, menu `Account` en haut à droite.
2. Une fois connecté avec vos identifiants de TP, cliquez sur le bouton bleu `SSH` (il y a 4 boutons bleus : `Information`, `SSH`, `Details`, et `History`).
3. Copiez le contenu de votre **clé publique** dans la partie `Add public SSH key` et n'oubliez pas de cliquer sur `Add`, ce n'est pas automatique.

Votre clé publique étant configurée, vous pouvez maintenant vous connecter à Genocluster avec `ssh`.

## Se connecter à Genocluster

Pour vous connecter à Genocluster, nous allons utiliser la commande `ssh`. La commande `ssh` s'utilise en fournissant le login et l'url du serveur. Dans votre cas, le login est celui fourni pour le TP et l'adresse url de genocluster est `genossh.genouest.org`. Je vous invite donc à :
1. ouvrir un terminal
2. taper `ssh votrelogin@genossh.genouest.org`

Si votre clé est bien ajoutée, vous arrivez normalement sur le *frontal* de genocluster. Vous pouvez voir le *frontal* comme l'accueil d'un hôtel, c'est ici que vous allez demander votre numéro de chambre, c-à-d demander l'accès à un **noeud** de calcul (autrement dit un ordinateur du cluster). Tout comme on ne dort pas dans l'accueil de l'hôtel, **on n'exécute pas de commande sur le frontal**.

Donc dès votre arrivée sur genocluster, vous allez demander d'accéder à un **noeud** avec la commande `srun`.

```bash
[tp57656@genossh:tp_snakemake] $ srun --pty bash
[tp57656@cl1n021:tp_snakemake] $ 
```

Sur la première ligne, vous voyez que je suis sur le *frontal* (genossh) et sur la deuxième ligne, je suis maintenant sur un noeud de calcul comme l'indique le *prompt* ; dans mon cas (`[tp57656@cl1n021:tp_snakemake]`), je suis sur le noeud numéro 21 du cluster 1.

`srun` est une commande du gestionnaire de tâches [SLURM](https://slurm.schedmd.com), qui est utilisé sur Genocluster pour ordonnancer les tâches des différents utilisateurs. C'est un peu le réceptionniste qui vous attribue votre numéro de chambre (et des couvertures en plus si vous le demandez).

Un résumé des différentes commandes SLURM est disponible dans ce document [summary.pdf](https://slurm.schedmd.com/pdfs/summary.pdf) mais nous allons en voir quelques-unes ensemble.

## Récupérer les fichiers du TP

Je vous propose tout d'abord de récupérer les fichiers du TP via `git`. Pour cela, vous devez cloner le répertoire : `git@gitlab.com:gcollet/tp_snakemake.git`. Malheureusement, cela ne fonctionnera pas... Je vous propose de réfléchir là-dessus (indice : relisez vos notes sur les clés ssh). Nous verrons cela en TP.

## Exécuter des commandes

Lorsque vous voulez exécuter une commande, vous **devez** écrire un script bash qui contient la configuration demandée (nombre de processeur, quantité de mémoire, etc) et la commande que vous souhaitez exécuter. Ensuite, SLURM place votre demande en **file d'attente** et se charge de trouver de la place sur le cluster. Quand l'exécution de votre commande est possible, SLURM lance les calculs.

Par exemple, vous voulez exécuter la commande `ls` (aucun intérêt mais c'est pour l'exemple). Vous créez un script `lance_ls.sh` dans lequel vous écrivez :

```bash
#!/bin/bash

ls
```

Pour demander à SLURM d'exécuter votre script, il faut utiliser la commande :
```bash
sbatch lance_ls.sh
```

Normalement, SLURM affecte un numéro de job à votre demande (dans notre cas `Submitted batch job 1527902`) et lorsque la commande sera lancée, vous verrez apparaître un fichier `slurm-1527902.out`dans le répertoire où vous avez lancé le `sbatch`


Évidemment, l'exécution de votre commande ne se fait pas dans un bash interactif où vous voyez les informations s'afficher au fur et à mesure. Votre commande se lance sur un ordi distant, sans que vous ayez la main dessus. Ainsi, pour voir le résultat de votre commande, SLURM crée deux fichiers : l'un qui contient ce qui s'affiche sur la sortie standard, l'autre sur l'erreur standard.
